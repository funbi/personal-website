const Projects = () => (
  <div>
    <h3>Projects</h3>
    <div></div>
    <p>
      I work a lot of side projects, most of which go nowhere. But this is a
      full list of those that survived.
    </p>
    <style global jsx>
      {`
        body {
          background-color: #000;
          color: #bdbdbd;
          font-family: "Nanum Gothic Coding", monospace;
          font-size: 16px;
          margin: 0 1.5rem;
        }
      `}
    </style>
  </div>
);

export default Projects;
