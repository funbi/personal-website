import Link from "next/link";
const HomePage = () => (
  <div>
    <h3>Hi, I'm Funbi</h3>

    <h4>You would be amaze if you know me.</h4>
    <div>
      <ul>
        <li>
          <Link href="/about">
            <a>/about</a>
          </Link>
        </li>
        <li>
          <Link href="/projects">
            <a>/projects</a>
          </Link>
        </li>
        <li>
          <Link href="/notes">
            <a>/notes</a>
          </Link>
        </li>
        <li>
          <Link href="https://twitter.com/Funbiiii">
            <a>/twitter</a>
          </Link>
        </li>
        <li>
          <Link href="https://github.com/FunbiOyede">
            <a>/github</a>
          </Link>
        </li>
        <li>
          <Link href="cv">
            <a>/cv</a>
          </Link>
        </li>
        <li>
          <Link href="mailto:oyedetobi49@gmail.com">
            <a>/mail</a>
          </Link>
        </li>
      </ul>
    </div>
    <footer>
      <div>
        <h5>(c) funbioyede;</h5>
      </div>
    </footer>
    <style global jsx>
      {`
        body {
          background-color: #000;
          color: #bdbdbd;
          font-family: "Nanum Gothic Coding", monospace;
          font-size: 16px;
          margin: 0 1.5rem;
        }
      `}
    </style>
  </div>
);

export default HomePage;
